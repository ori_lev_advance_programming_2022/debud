
#define _CRT_SECURE_NO_WARNINGS

#include "part1.h"
#include <iostream>



void part1()
{
	char password[] = "secret";
	char dest[13] = {0};
	char src[] = "hello world!";

	strcpy(dest, src);

	std::cout << src << std::endl;
	std::cout << dest << std::endl;
}
